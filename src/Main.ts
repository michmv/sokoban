///<reference path="../node_modules/@types/jquery/index.d.ts" />

import * as T from "./Types"
import * as L from "./Levels"
import * as THREE from "../three/Three"
import * as A from "./App"
import * as AA from "./Animation"

$(document).ready(function() {
    let app: T.Application = initApp()

    $(window).resize( () => resize(app) )

    $(document).keydown((e) => {
        if(e.which == 37) {
            A.move(app, [-1,0]) // left
        } else if(e.which == 38) {
            A.move(app, [0,-1]) // top
        } else if(e.which == 39) {
            A.move(app, [1,0]) // right
        } else if(e.which == 40) {
            A.move(app, [0,1]) // down
        } else if(e.which == 189) {
            A.changeLevel(app, T.ChangeLevel.Minus) // minus
        } else if(e.which == 187) {
            A.changeLevel(app, T.ChangeLevel.Plus) // plus
        } else if(e.which == 82) {
            A.changeLevel(app, T.ChangeLevel.Self) // r
        } else if(e.which == 90) {
            A.backStep(app) // z
        }
    })

    initControlElement(app)

    A.run(app)

    function loop(time: number): void {
        app.clock = time

        let update = false

        for(let i=0; i<app.animations.length; i++) {
            let animation = app.animations[i]
            for(let j=0; j<animation.list.length; j++) {
                update = true
                let item_animation = animation.list[j]
                item_animation.func(
                    animation.object,
                    item_animation.animation.animate(app.clock)
                )
            }
        }

        if(update) {
            A.updateScene(app)
        }

        window.requestAnimationFrame(loop)
    }

    window.requestAnimationFrame(loop)
})

function initApp(): T.Application {
    return {
        widthWindow: 0,
        heightWindow: 0,
        level: 1,
        level_preselect: {
            level: 0,
            max: L.levels.length,
            width_line: 0,
            left_line: 0,
            width_item: 0,
        },
        html: {
            play: $('#play'),
            canvas: document.getElementById('canvas'),
            play_button: $('#play_button'),
            play_level: $('#play_level'),
            play_step: $('#play_step'),
            message_win: $('#layout_message_win'),
            level_select: $('#layout_level_select')
        },
        webgl: {
            renderer: undefined,
            scene: undefined,
            camera: undefined,
            objects: {
                walls: [],
                boxs: [],
                targets: [],
                player: undefined,
                base: undefined
            },
            materials: {
                base: new THREE.MeshLambertMaterial({color: 0xb0b0b0}),
                wall: new THREE.MeshLambertMaterial({color: 0xdddddd}),
                box: new THREE.MeshLambertMaterial({color: 0xffaaaa}),
                box_finish: new THREE.MeshLambertMaterial({color: 0xaaffaa}),
                target: new THREE.MeshLambertMaterial({color: 0xffff88}),
                player: new THREE.MeshLambertMaterial({color: 0x4444ff})
            },
            sizeGeometry: 20,
            sizePlayerY: 20 * 1.5,
            sizeBox: 20 - 2,
            sizeBoxHypotenuse: Math.sqrt((20-2) * (20-2) * 2)
        },
        mode: T.Mode.Init,
        steps: [],
        clock: 0,
        animations: [],
        duration: 200
    }
}

function initControlElement(app: T.Application): void {
    app.widthWindow = window.innerWidth
    app.heightWindow = window.innerHeight
    initScene(app)

    // смена уровня
    app.html.play_level.children('#level_plus').click(
        () => A.changeLevel(app, T.ChangeLevel.Plus) )
    app.html.play_level.children('#level_minus').click(
        () => A.changeLevel(app, T.ChangeLevel.Minus) )
    
    // с начала
    app.html.play_button.children('#reset').click(
        () => A.changeLevel(app, T.ChangeLevel.Self) )

    // выбор уровня
    app.html.play_button.children('#other_level').click(
        () => A.showLayoutLevelSelect(app) )

    // отмена хода
    app.html.play_step.children('#back').click(
        () => A.backStep(app))

    // следующий уровень в сообщении о победе
    $('.next_level .button', app.html.message_win).click(function(){
            app.mode = T.Mode.Play
            A.changeLevel(app, T.ChangeLevel.Plus)
            A.hideLayoutWin(app)
        })

    // показать выбор уровня после прохождения последнего уровня
    $('.continue', app.html.message_win).click(function(){
        app.mode = T.Mode.LevelSelect
        A.hideLayoutWin(app)
        A.showLayoutLevelSelect(app)
    })

    // меню выбор уровня

    $('#level_select_item', app.html.level_select).mousedown(function(){
        app.mode = T.Mode.LevelSelectMouseDown
    })

    $(document).mousemove(function(e){
        if(app.mode == T.Mode.LevelSelectMouseDown) {
            let p = e.pageX - app.level_preselect.left_line
            if(p < 0) p = 0
            if(p > app.level_preselect.width_line)
                p = app.level_preselect.width_line

            app.level_preselect.level = Math.round(p / (
                app.level_preselect.width_line / (app.level_preselect.max - 1)
                )) + 1

            A.updateLevelSelectIndicator(app)
            
            $('#level_select_item', app.html.level_select).css(
                'left',
                p - app.level_preselect.width_item / 2 + 'px'
            )
        }
    }).mouseleave(function(){
        if(app.mode == T.Mode.LevelSelectMouseDown) {
            A.updateCursorLevelSelect(app)
            app.mode = T.Mode.LevelSelect
        }
    }).mouseup(function(){
        if(app.mode == T.Mode.LevelSelectMouseDown) {
            A.updateCursorLevelSelect(app)
            app.mode = T.Mode.LevelSelect
        }
    })

    // обработчик + и -

    $('#level_select_minus', app.html.level_select).click(function(){
        A.levelPreSelectChange(app, T.ChangeLevel.Minus)
    })

    $('#level_select_plus', app.html.level_select).click(function(){
        A.levelPreSelectChange(app, T.ChangeLevel.Plus)
    })

    // повесить обработчик на кнопку выбора и отмены выбора

    $('#level_select_cancel', app.html.level_select).click(function(){
        A.hideLayoutLevelSelect(app)
    })

    $('#level_select_ok', app.html.level_select).click(function(){
        A.hideLayoutLevelSelect(app)
        A.setLevel(app, app.level_preselect.level)
        app.mode = T.Mode.Play
    })
}

/**
 * Создать 3Д сцену
 */
function initScene(app: T.Application): void {
    app.webgl.renderer = new THREE.WebGLRenderer(
        {canvas: <HTMLCanvasElement>app.html.canvas, antialias: true})
    app.webgl.renderer.setClearColor(0xffffff)
    app.webgl.renderer.shadowMap.enabled = true;
    app.webgl.renderer.shadowMap.type = THREE.PCFSoftShadowMap

    app.webgl.scene = new THREE.Scene()

    app.webgl.camera = new THREE.PerspectiveCamera(45, 1, 0.1, 5000)
    app.webgl.camera.position.set(0, 275, 275)
    app.webgl.camera.lookAt(new THREE.Vector3(0, -55, 0))

    let targetObject = new THREE.Object3D();
    app.webgl.scene.add(targetObject)

    let light = new THREE.SpotLight(0xffffff, 1)
    light.position.set(-5000, 4500, 4000)
    light.castShadow = true
    light.penumbra = 0
    light.target = targetObject
    light.castShadow = true
    app.webgl.scene.add(light)

    let light2 = new THREE.SpotLight(0xffffff, 0.9)
    light2.position.set(5000, 4500, 4000)
    light2.castShadow = true
    light2.penumbra = 0
    light2.target = targetObject
    app.webgl.scene.add(light2)
}

function resize(app: T.Application): void {
    app.widthWindow = window.innerWidth
    app.heightWindow = window.innerHeight
    A.setSizeLayoutPlay(app)
    A.setSizeLayoutMessageWin(app)
    A.setSizeLayoutLevelSelect(app)
}
