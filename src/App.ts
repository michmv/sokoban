///<reference path="../node_modules/@types/jquery/index.d.ts" />

import * as THREE from "../three/Three"
import * as L from "./Levels"
import * as T from "./Types"
import * as M from "./Maybe"
import * as A from "./Animation"

export function run(app: T.Application): void {
    loadScene(app)
    showLayoutPlay(app)
    app.mode = T.Mode.Play
}

export function changeLevel(app: T.Application, action: T.ChangeLevel): void {
    if(app.mode != T.Mode.Play) return

    let new_level = app.level

    if(action == T.ChangeLevel.Plus) new_level++
    else if(action == T.ChangeLevel.Minus) new_level--

    setLevel(app, new_level)

    app.mode = T.Mode.Play
}

export function setSizeLayoutPlay(app: T.Application): void {
    app.html.play.width(app.widthWindow).height(app.heightWindow)

    setSize3DScene(app)
    setSizePositionButton(app)
    setSizePositionLevel(app)
    setSizePositionSteps(app)
}

export function setSizeLayoutMessageWin(app: T.Application): void {
    let box = app.html.message_win.children('#message_win_box')
    let font_size = getFontSize(app)

    box.css({
        fontSize: font_size + 'px',
        padding: font_size * 3 + 'px'
    })
    box.children('.title').css({
        fontSize: font_size * 1.4 + 'px',
        paddingBottom: font_size + 'px'
    })
    box.children('.message').css({paddingBottom: font_size + 'px'})
    $('.button', box).css({
        fontSize: font_size + 'px',
        lineHeight: font_size * 1.6 + 'px',
        padding: '0 ' + font_size * 0.5 + 'px'
    })

    setSizeLayoutAndPositionWindow(app, box)
}

export function setSizeLayoutLevelSelect(app: T.Application): void {
    let box = app.html.level_select.children('#level_select_box')
    let font_size = getFontSize(app)

    setSizeLevelSelectTitleButton(app)

    setSizeLevelSelectControl(app)

    setSizeLayoutAndPositionWindow(app, box)

    app.level_preselect.left_line = $('#level_select_line', box)
        .offset().left
}

export function move(app: T.Application, v: T.Vector): void {
    if(app.mode != T.Mode.Play) return

    let list_move: Array<T.MoveCommand> = createMoveCommand(app, v)

    if(list_move.length > 0) {
        app.steps.push(list_move)
        finishAnimation(app)
        app.animations = createAmimation(app, list_move)

        updateStepIndicator(app)

        if(checkFinishLevel(app)) {
            app.mode = T.Mode.Win
            showLayoutWin(app)
        }
    }
}

export function backStep(app: T.Application): void {
    if(app.mode != T.Mode.Play || app.steps.length == 0) return

    app.mode = T.Mode.Back
    let list_move = app.steps[app.steps.length - 1]
    let tmp_point: T.Point

    for(let i=0; i<list_move.length; i++) {
        list_move[i].object.x = list_move[i].old_point.x
        list_move[i].object.y = list_move[i].old_point.y
        tmp_point = list_move[i].point
        list_move[i].point = list_move[i].old_point
        list_move[i].old_point = tmp_point

        updateBoxMaterial(app, list_move[i].object, list_move[i].point)
    }

    finishAnimation(app)
    app.animations = createAmimation(app, list_move)

    app.steps = app.steps.slice(0, -1)
    updateStepIndicator(app)

    app.mode = T.Mode.Play
}

export function showLayoutLevelSelect(app: T.Application): void {
    app.mode = T.Mode.LevelSelect
    app.level_preselect.level = app.level
    setSizeLayoutLevelSelect(app)
    updateLevelSelect(app)
    app.html.level_select.css('top', 0)
}

export function hideLayoutWin(app: T.Application): void {
    app.html.message_win.css('top', '-5000px')
}

export function hideLayoutLevelSelect(app: T.Application): void {
    app.html.level_select.css('top', '-5000px')
    app.mode = T.Mode.Play
}

export function levelPreSelectChange(app: T.Application, action: T.ChangeLevel): void {
    let level = app.level_preselect.level

    if(action == T.ChangeLevel.Minus) level--
    else if(action == T.ChangeLevel.Plus) level++
    else return

    if(1 <= level && level <= app.level_preselect.max) {
        app.level_preselect.level = level
        updateLevelSelect(app)
    }
}

export function setLevel(app: T.Application, n: number): void {
    if(1 <= n && n <= L.levels.length) {
        app.level = n
        loadScene(app)
    }
}

export function updateLevelSelectIndicator(app: T.Application): void {
    $('#level_select_number span', app.html.level_select)
        .text(String(app.level_preselect.level))
}

export function updateCursorLevelSelect(app: T.Application):void {
    let x: number = 0
    let cursor = app.level_preselect

    if(cursor.max == 1) x = cursor.width_line / 2
    else {
        x = cursor.width_line / (cursor.max - 1) * (cursor.level - 1)
            - cursor.width_item / 2
    }
    
    $('#level_select_item', app.html.level_select).css('left', x + 'px')
}

export function updateScene(app: T.Application): void {
    if(app.webgl.renderer !== undefined)
        app.webgl.renderer.render(app.webgl.scene, app.webgl.camera)
}

//-------------------------------------------------------------------------------

function loadScene(app: T.Application): void {
    // обновить элементы управления
    updateLevelIndicator(app)
    app.steps = []
    app.animations = []
    updateStepIndicator(app)

    let level = loadLevel(app)

    // удалить объекты с 3Д сцены
    deleteElementFromScene(app)

    // построить новую сцену
    createBaseOnScene(app, level.width, level.height)
    createObjectsOnScene(app)

    updateScene(app)
}

function createObjectsOnScene(app: T.Application): void {
    let level = loadLevel(app)
    let t_object: T.Object
    let field: string
    let x: number
    let y: number

    for(let i=0; i<level.field.length; i++) {
        field = level.field[i]
        x = i % level.width
        y = Math.floor(i / level.width)

        if(field == '#') {
            t_object = createWall(app, x, y, level.width, level.height)
            app.webgl.objects.walls.push(t_object)
            app.webgl.scene.add(t_object.mesh)
        } else if(field == '$') {
            t_object = createBox(app, x, y, level.width, level.height)
            app.webgl.objects.boxs.push(t_object)
            app.webgl.scene.add(t_object.mesh)
        } else if(field == '.') {
            t_object = createTarget(app, x, y, level.width, level.height)
            app.webgl.objects.targets.push(t_object)
            app.webgl.scene.add(t_object.mesh)
        } else if(field == '@') {
            t_object = createPlayer(app, x, y, level.width, level.height)
            app.webgl.objects.player = t_object
            app.webgl.scene.add(t_object.mesh)
        }
    }
}

function createPlayer(app: T.Application, x: number, y: number, w: number, h: number): T.Object {
    let size = app.webgl.sizeGeometry
    let geometry = new THREE.ConeGeometry(size / 2 - 1, app.webgl.sizePlayerY, 32)
    let mesh = new THREE.Mesh(geometry, app.webgl.materials.player)
    mesh.position.x = x * size - (w - 1) * size / 2
    mesh.position.y = app.webgl.sizePlayerY / 2
    mesh.position.z = y * size - (h - 1) * size / 2
    mesh.receiveShadow = true
    mesh.castShadow = true
    return {mesh: mesh, x: x, y: y, type: T.ObjectType.Player}
}

function createTarget(app: T.Application, x: number, y: number, w: number, h: number): T.Object {
    let size = app.webgl.sizeGeometry
    let geometry = new THREE.BoxGeometry(2, size, 2)
    let mesh = new THREE.Mesh(geometry, app.webgl.materials.target)
    mesh.position.x = x * size - (w - 1) * size / 2
    mesh.position.y = size / 2.5
    mesh.position.z = y * size - (h - 1) * size / 2
    mesh.receiveShadow = true
    mesh.castShadow = true
    return {mesh: mesh, x: x, y: y, type: T.ObjectType.Target}
}

function createBox(app: T.Application, x: number, y: number, w: number, h: number): T.Object {
    let size = app.webgl.sizeGeometry
    let size_box = app.webgl.sizeBox
    let geometry = new THREE.BoxGeometry(size_box, size_box, size_box)
    let mesh = new THREE.Mesh(geometry, app.webgl.materials.box)
    mesh.position.x = x * size - (w - 1) * size / 2
    mesh.position.y = size_box / 2
    mesh.position.z = y * size - (h - 1) * size / 2
    mesh.receiveShadow = true
    mesh.castShadow = true
    return {mesh: mesh, x: x, y: y, type: T.ObjectType.Box}
}

function createWall(app: T.Application, x: number, y: number, w: number, h: number): T.Object {
    let size = app.webgl.sizeGeometry
    let geometry = new THREE.BoxGeometry(size, size / 2, size)
    let mesh = new THREE.Mesh(geometry, app.webgl.materials.wall)
    mesh.position.x = x * size - (w - 1) * size / 2
    mesh.position.y = size / 4
    mesh.position.z = y * size - (h - 1) * size / 2
    mesh.receiveShadow = true
    mesh.castShadow = true
    return {mesh: mesh, x: x, y: y, type: T.ObjectType.Wall}
}

function loadLevel(app: T.Application): T.Field {
    return L.levels[app.level - 1]
}

function createBaseOnScene(app: T.Application, width: number, height: number): void {
    let size = app.webgl.sizeGeometry
    let geometry = new THREE.BoxGeometry(width * size, 4, height * size)
    let base = new THREE.Mesh(geometry, app.webgl.materials.base)
    base.receiveShadow = true
    base.position.y = -2
    base.castShadow = true
    app.webgl.objects.base = base
    app.webgl.scene.add(base)
}

function deleteElementFromScene(app: T.Application): void {
    let scene = app.webgl.scene
    let objects = app.webgl.objects

    for(let i=0; i<objects.walls.length; i++) {
        scene.remove(objects.walls[i].mesh)
    }
    objects.walls = []

    for(let i=0; i<objects.boxs.length; i++) {
        scene.remove(objects.boxs[i].mesh)
    }
    objects.boxs = []

    for(let i=0; i<objects.targets.length; i++) {
        scene.remove(objects.targets[i].mesh)
    }
    objects.targets = []

    if(objects.player !== undefined) {
        scene.remove(objects.player.mesh)
        objects.player = undefined
    }

    if(objects.base !== undefined) {
        scene.remove(objects.base)
        objects.base = undefined
    }
}

function updateStepIndicator(app: T.Application): void {
    app.html.play_step.children('#step_n').children('span')
        .text(app.steps.length)
}

function updateLevelIndicator(app: T.Application): void {
    let number = '00' + app.level
    app.html.play_level.children('#level_n').children('span')
        .text(number.substr(-2,2))
}

function updateMessageWin(app: T.Application): void {
    let box = app.html.message_win.children('#message_win_box')
    if(app.level == L.levels.length) {
        box.children('.next_level').css('display', 'none')
        box.children('.last_level').css('display', 'block')
    } else {
        box.children('.next_level').css('display', 'block')
        box.children('.last_level').css('display', 'none')
    }
}

function showLayoutPlay(app: T.Application): void {
    setSizeLayoutPlay(app)
    app.html.play.css('top', 0)
}

function showLayoutWin(app: T.Application): void {
    updateMessageWin(app)
    setSizeLayoutMessageWin(app)
    app.html.message_win.css('top', 0)
}

/**
 * Вычислить размеры для отображения 3Д сцены
 */
function callSize3D(width: number, height: number): T.Size {
    let k = 16/9
    let h = width / k
    if(h < height) return {width: width, height: h}
    else return {width: height * k, height: height}
}

function getFontSize(app: T.Application): number {
    let size = callSize3D(app.widthWindow, app.heightWindow)
    return size.height * 3.5 / 100
}

function setSize3DScene(app: T.Application): void {
    let size = callSize3D(app.widthWindow, app.heightWindow)

    if(app.webgl.camera !== undefined) {
        app.webgl.camera.aspect = size.width / size.height
        app.webgl.camera.updateProjectionMatrix()
    }

    if(app.webgl.renderer !== undefined) {
        app.webgl.renderer.setSize(size.width, size.height)
    }

    app.html.canvas.setAttribute('width', String(size.width))
    app.html.canvas.setAttribute('height', String(size.height))
    app.html.canvas.style.left = app.widthWindow / 2 - size.width / 2 + 'px'
    app.html.canvas.style.top = app.heightWindow / 2 - size.height / 2 + 'px'

    updateScene(app)
}

function setSizePositionButton(app: T.Application): void {
    let height_text = getFontSize(app)

    app.html.play_button.height(height_text * 1.6).css({bottom: height_text + 'px'})
    app.html.play_button.children('.item').css({
        fontSize: height_text + 'px',
        lineHeight: height_text * 1.6 + 'px',
        padding: '0 ' + height_text * 0.5 + 'px',
        margin: '0 ' + height_text + 'px'
    })
}

function setSizePositionLevel(app: T.Application): void {
    let height_text = getFontSize(app)

    app.html.play_level.height(height_text).css({
        left: height_text + 'px',
        top: height_text + 'px'
    })
    app.html.play_level.children('.item').css({
        fontSize: height_text + 'px',
        lineHeight: height_text * 1.6 + 'px',
        marginRight: height_text / 2 + 'px'
    })
    app.html.play_level.children('.button').css({
        padding: '0 ' + height_text * 0.5 + 'px'
    })
}

function setSizePositionSteps(app: T.Application): void {
    let height_text = getFontSize(app)

    app.html.play_step.height(height_text).css({
        right: height_text + 'px',
        top: height_text + 'px'
    })
    app.html.play_step.children('.item').css({
        fontSize: height_text + 'px',
        lineHeight: height_text * 1.6 + 'px',
        marginRight: height_text / 2 + 'px'
    })
    app.html.play_step.children('.button').css({
        padding: '0 ' + height_text * 0.5 + 'px'
    })
}

function pointPlusVector(p: T.Point, v: T.Vector): T.Point {
    return {x: p.x + v[0], y: p.y + v[1]}
}

function getPointObject(obj: T.Object): T.Point {
    return {x: obj.x, y: obj.y}
}

function findObjectByPoint(app: T.Application, p: T.Point): M.Maybe<T.Object> {
    let result: M.Maybe<T.Object>

    result = findObjectByPointInList(app.webgl.objects.walls, p)
    if(result instanceof M.Just) return result

    return findObjectByPointInList(app.webgl.objects.boxs, p)
}

function findObjectByPointInList(list: Array<T.Object>, p: T.Point): M.Maybe<T.Object> {
    for(let i=0; i<list.length; i++) {
        if(list[i].x == p.x && list[i].y == p.y)
            return new M.Just(list[i])
    }
    return new M.Nothing
}

function updateBoxMaterial(app: T.Application, object: T.Object, p: T.Point): void {
    if(object.type != T.ObjectType.Box) return

    let target = findObjectByPointInList(app.webgl.objects.targets, p)
    if(target instanceof M.Just) {
        object.mesh.material = app.webgl.materials.box_finish
    } else {
        object.mesh.material = app.webgl.materials.box
    }
}

function setPositionOnScene(app: T.Application, mesh: THREE.Mesh, p: T.Point): void {
    let level = loadLevel(app)
    let size = app.webgl.sizeGeometry
    mesh.position.x = p.x * size - (level.width - 1) * size / 2
    mesh.position.z = p.y * size - (level.height -1) * size / 2
}

function checkFinishLevel(app: T.Application): boolean {
    let boxs = app.webgl.objects.boxs
    let target = app.webgl.objects.targets
    let find: M.Maybe<T.Object>
    let p: T.Point

    for(let i=0; i<boxs.length; i++) {
        p = {x: boxs[i].x, y: boxs[i].y}
        find = findObjectByPointInList(target, p)
        if(find instanceof M.Nothing) return false
    }

    return true
}

function createMoveCommand(app: T.Application, v: T.Vector): Array<T.MoveCommand> {
    let result = []
    let player = app.webgl.objects.player
    let new_point = pointPlusVector(getPointObject(player), v)
    let obj_nearby = findObjectByPoint(app, new_point)

    if(obj_nearby instanceof M.Just) {
        let obj: T.Object = obj_nearby.get()
        if(obj.type == T.ObjectType.Box) {
            let new_point2 = pointPlusVector(getPointObject(obj), v)
            obj_nearby = findObjectByPoint(app, new_point2)
            if(obj_nearby instanceof M.Nothing) {
                result.push({
                    object: player,
                    point: new_point,
                    old_point: {x: player.x, y: player.y}
                })
                result.push({
                    object: obj,
                    point: new_point2,
                    old_point: {x: obj.x, y: obj.y}
                })

                updateBoxMaterial(app, obj, new_point2)
            }
        }
    } else {
        result.push({
            object: player,
            point: new_point,
            old_point: {x: player.x, y: player.y}
        })
    }

    for(let i=0; i<result.length; i++) {
        result[i].object.x = result[i].point.x
        result[i].object.y = result[i].point.y
    }

    return result
}

function setSizeLevelSelectTitleButton(app: T.Application): void {
    let box = app.html.level_select.children('#level_select_box')
    let font_size = getFontSize(app)

    box.css({
        fontSize: font_size + 'px',
        padding: font_size * 3 + 'px'
    })
    box.children('#level_select_number').css({
        fontSize: font_size * 1.4 + 'px',
        paddingBottom: font_size + 'px'
    })
    box.children('#level_select_input').width(font_size * 20)
    $('#level_select_button .button', box).css({
        fontSize: font_size + 'px',
        lineHeight: font_size * 1.6 + 'px',
        padding: '0 ' + font_size * 0.5 + 'px'
    })
    $('#level_select_ok').css('margin-left', font_size + 'px')
}

function setSizeLevelSelectControl(app: T.Application): void {
    let box = app.html.level_select.children('#level_select_box')
    let font_size = getFontSize(app)

    let input_size = font_size * 1.5
    let width_line = font_size * 16
    let width_item = input_size / 2

    let input = box.children('#level_select_input')
    input.css({
        height: input_size + 'px',
        paddingBottom: input_size + 'px',
    })
    input.children('.button').css({
        width: input_size + 'px', 
        height: input_size + 'px',
        fontSize: input_size + 'px',
        lineHeight: input_size + 'px'
    })
    input.children('#level_select_line').css({
        width: width_line + 'px',
        height: input_size + 'px',
        left: font_size * 2 + 'px',
    })
    let fon_height = input_size * 0.1
    if(fon_height < 2) fon_height = 2
    $('#level_select_fon', input).css({
        top: input_size / 2 - fon_height / 2 + 'px',
        height: fon_height
    })
    $('#level_select_item', input).css({
        width: width_item + 'px',
        height: input_size + 'px',
        left: '-9999px'
    })

    app.level_preselect.width_line = width_line
    app.level_preselect.width_item = width_item

    updateCursorLevelSelect(app)
}

function setSizeLayoutAndPositionWindow
    ( app: T.Application, window_obj: JQuery<HTMLElement>): void {
    app.html.level_select.css({
        width: app.widthWindow + 'px',
        height: app.heightWindow + 'px'
    })

    let w = window_obj.outerWidth()
    let h = window_obj.outerHeight()
    window_obj.css({
        left: app.widthWindow / 2 - w / 2 + 'px',
        top: app.heightWindow / 2 - h /2 + 'px'
    })
}

function updateLevelSelect(app: T.Application): void {
    // установить текущий уровень
    updateLevelSelectIndicator(app)

    // обновить позиция бегунка
    updateCursorLevelSelect(app)
}

function createAmimation(app: T.Application, list: Array<T.MoveCommand>):
    Array<T.AnimationObject>
    {
    let result = []

    for(let i=0; i<list.length; i++) {
        if(list[i].object.type == T.ObjectType.Player) {
            result.push(createAnimationPlayer(app, list[i]))
        } else {
            result.push(createAnimationBox(app, list[i]))
        }
    }

    return result
}

function createAnimationPlayer(app: T.Application, command: T.MoveCommand):
    T.AnimationObject
    {
    let list = []
    list.push(createAnimationMove(app, command))

    // up/down
    list.push({
        func: (obj, value) => { obj.mesh.position.y = value },
        animation: A.animation(app.clock)
            .from(app.webgl.sizePlayerY / 2)
            .to(app.webgl.sizePlayerY / 2 + 5)
            .duration(app.duration)
            .easy((value: number) => {
                if(value <= 0.5) return value * 2
                else return (1 - value) * 2
            })
    })

    return {object: command.object, list: list}
}

function createAnimationBox(app: T.Application, command: T.MoveCommand):
    T.AnimationObject
    {
    let list = []
    let funcY = (obj, value) => { obj.mesh.position.y = value }
    let offset = (app.webgl.sizeBoxHypotenuse - app.webgl.sizeBox) / 2

    list.push(createAnimationMove(app, command))
    list.push(createAnimationRotate(app, command))

    // up/down
    list.push({
        func: funcY,
        animation: A.animation(app.clock)
            .from(app.webgl.sizeBox / 2)
            .to(app.webgl.sizeBox / 2 + offset)
            .duration(app.duration)
            .easy((value: number) => {
                let result: number
                if(value <= 0.5) result = value * 2
                else result = (1 - value) * 2
                return result
            })
    })

    return {object: command.object, list: list}
}

function createAnimationRotate(app: T.Application, command: T.MoveCommand):
    T.Animation
    {
    let func: (obj: T.Object, value: number) => void
    let animation: A.Animation
    let start: number
    let angle = 90 * Math.PI / 180

    if(command.point.x != command.old_point.x) {
        // вращать по оси z
        func = (obj, value) => { obj.mesh.rotation.z = value }
        if(command.point.x > command.old_point.x) {
            start = angle
        } else {
            start = angle * -1
        }
    } else {
        // вращать по оси x
        func = (obj, value) => { obj.mesh.rotation.x = value }
        if(command.point.y > command.old_point.y) {
            start = angle * -1
        } else {
            start = angle
        }
    }

    animation = A.animation(app.clock)
        .from(start)
        .to(0)
        .duration(app.duration)

    return {func: func, animation: animation}
}

function createAnimationMove(app: T.Application, command: T.MoveCommand):
    T.Animation
    {
    let func: (obj: T.Object, value: number) => void
    let animation: A.Animation
    let start: number, finish: number

    let size = app.webgl.sizeGeometry
    let level = loadLevel(app)

    if(command.point.x != command.old_point.x) {
        func = (obj, value) => { obj.mesh.position.x = value }
        start = command.old_point.x * size - (level.width - 1) * size / 2
        finish = command.point.x * size - (level.width - 1) * size / 2
    } else {
        func = (obj, value) => { obj.mesh.position.z = value }
        start = command.old_point.y * size - (level.height - 1) * size / 2
        finish = command.point.y * size - (level.height - 1) * size / 2
    }

    animation = A.animation(app.clock)
        .from(start)
        .to(finish)
        .duration(app.duration)

    return {func: func, animation: animation}
}

function finishAnimation(app: T.Application): void {
    for(let i=0; i<app.animations.length; i++) {
        let animation = app.animations[i]
        for(let j=0; j<animation.list.length; j++) {
            let item_animation = animation.list[j]
            item_animation.func(
                animation.object,
                A.finish(item_animation.animation).animate(app.clock)
            )
        }
    }

    app.animations = []
}
