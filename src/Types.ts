///<reference path="../node_modules/@types/jquery/index.d.ts" />

import * as THREE from "../three/Three"
import * as A from "./Animation"

export interface Size
    { width: number
    , height: number
    }

export interface Point
    { x: number
    , y: number
    }

// [x= -1...1, y= -1...1]
export type Vector = [number, number]

export interface Application
    { widthWindow: number
    , heightWindow: number
    , level: number
    , level_preselect: PreSelect
    , html: HtmlObject
    , webgl: WebGL
    , mode: Mode
    , steps: Array<Array<MoveCommand>>
    , clock: number
    , animations: Array<AnimationObject>
    , duration: number
    }

export interface AnimationObject
    { object: Object
    , list: Array<Animation>
    }

export interface Animation
    { func: (obj: Object, value: number) => void
    , animation: A.Animation
    }

export interface PreSelect
    { level: number
    , max: number
    , width_line: number
    , left_line: number
    , width_item: number
    }

export enum ChangeLevel {Plus, Minus, Self}

export enum Mode {Play, Init, ChangeLevel, Back, Win, LevelSelect, LevelSelectMouseDown}

export interface WebGL
    { renderer: THREE.WebGLRenderer
    , scene: THREE.Scene
    , camera: THREE.PerspectiveCamera
    , objects: Objects
    , materials: Materials
    , sizeGeometry: number
    , sizePlayerY: number
    , sizeBox: number
    , sizeBoxHypotenuse: number
    }

export interface Materials
    { base: THREE.MeshLambertMaterial
    , wall: THREE.MeshLambertMaterial
    , box: THREE.MeshLambertMaterial
    , box_finish: THREE.MeshLambertMaterial
    , target: THREE.MeshLambertMaterial
    , player: THREE.MeshLambertMaterial
    }

export interface Objects
    { walls: Array<Object>
    , boxs: Array<Object>
    , targets: Array<Object>
    , player: Object
    , base: THREE.Mesh
    }

export interface Object
    { mesh: THREE.Mesh
    , x: number
    , y: number
    , type: ObjectType
    }

export enum ObjectType { Wall, Box, Target, Player}

export interface MoveCommand
    { object: Object
    , point: Point
    , old_point: Point
    }

export interface HtmlObject
    { play: JQuery<HTMLElement>
    , canvas: HTMLElement
    , play_button: JQuery<HTMLElement>
    , play_level: JQuery<HTMLElement>
    , play_step: JQuery<HTMLElement>
    , message_win: JQuery<HTMLElement>
    , level_select: JQuery<HTMLElement>
    }

export interface Field
    { level: number
    , width: number
    , height: number
    , field: string
    }
