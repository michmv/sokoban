/**
 * Maybe
 * 
 * @version 1.0.0
 * @see <url>
 */

export abstract class Maybe<T> {
}

export class Just<T> extends Maybe<T> {
    constructor(private t: T) { super() }
    get(): T { return this.t }
}

export class Nothing<T> extends Maybe<T> {
}

export function fromMaybe<T>(default_value: T, value: Maybe<T>): T {
    if(value instanceof Just) return value.get()
    else return default_value
}
