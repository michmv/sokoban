/**
 * Animation
 * @version 1.0.0
 * 
 * import * as A from "./Animation"
 * 
 * let obj = {name: "obj_1", x:0}
 * 
 * let animation = A.animation(0).from(0).to(10).duration(5)
 * 
 * for(let time=0; time<11; time++) {
 *     console.log("time:" + time)
 *     obj.x = animation.animate(time)
 *     console.log(obj.name+":x="+obj.x)
 * }
 */

/**
 * Millisecond
 */
export type Clock = number

/**
 * Millisecond
 */
export type TimeDelta = number

/**
 * Create dynamic animation
 */
export function animation(clock: Clock): Animation {
    return new Animation(clock, TypeAnimation.Dynamic)
}

/**
 * Create fixed animation
 */
export function fixed(value: number): Animation {
    return new Animation(0, TypeAnimation.Fixed).value(value)
}

/**
 * Finish animation and fixed
 */
export function finish(animation: Animation): Animation {
    return fixed(animation.animate(
        animation.clock + animation.config.duration + animation.config.delay
    ))
}

/**
 * Cancel animation and fixed
 */
export function cancel(animation: Animation): Animation {
    return fixed(animation.animate(animation.clock))
}

export class Animation {
    config: Config =
        { duration: 1000
        , from: 0
        , to: 1
        , delay: 0
        , easy: (n: number) => { return n }
        }

    protected _value: number = 0

    constructor(public clock: Clock, protected _type: TypeAnimation) { return this }

    animate(clock: Clock): number {
        if(this.isFixed()) return this._value

        let delta = clock - this.clock - this.config.delay
        let status = this._status(delta)
        let k: number

        if(status == Status.Done) {
            k = 1
        } else {
            if(status == Status.Run) {
                if(delta > this.config.duration) delta = this.config.duration
                let speed = 1 / this.config.duration
                k = speed * delta
            } else {
                // status Schedule
                k = 0
            }
        }

        let distance = this.config.easy(k) * (this.config.to - this.config.from)
        let position = this.config.from + distance

        return position
    }

    /**
     * Delta >= 0. Default is 0
     */
    duration(delta: TimeDelta): Animation {
        this.config.duration = delta
        return this
    }

    from(from: number): Animation {
        this.config.from = from
        return this
    }

    to(to: number): Animation {
        this.config.to = to
        return this
    }

    /**
     * Delay >= 0. Default is 0
     */
    delay(delay: TimeDelta): Animation {
        this.config.delay = delay
        return this
    }

    /**
     * Set the easing function of an animation.
     * It is expected that f 0 == 0 and f 1 == 1.
     * The default is - (n: number) => { return n }
     */
    easy(easy: (value: number) => number): Animation {
        this.config.easy = easy
        return this
    }

    value(value: number): Animation {
        this._value = value
        return this
    }

    isFixed(): boolean {
        return this._type == TypeAnimation.Fixed
    }

    /**
     * Determine if an animation is scheduled, meaning that it has not yet changed value.
     */
    isScheduled(clock: Clock): boolean {
        if(this.isFixed()) return false
        return this._status(clock - this.clock - this.config.delay) == Status.Schedule
    }

    /**
     * Determine if an animation is running, meaning that it is currently changing value.
     * Fixed animations are never running.
     */
    isRunning(clock: Clock): boolean {
        if(this.isFixed()) return false
        return this._status(clock - this.clock - this.config.delay) == Status.Run
    }

    /**
     * Determine if an animation is done, meaning that it has arrived at its final value.
     * Fixed animations are always done.
     */
    isDone(clock: Clock): boolean {
        if(this.isFixed()) return true
        return this._status(clock - this.clock - this.config.delay) == Status.Done
    }

    protected _status(delta: TimeDelta): Status {
        if(delta <= 0)
            return Status.Schedule
        else if(delta >= this.config.duration)
            return Status.Done
        else
            return Status.Run
    }
}

export enum TypeAnimation {Fixed, Dynamic}

export interface Config
    { duration: TimeDelta
    , from: number
    , to: number
    , delay: TimeDelta
    , easy: (value: number) => number
    }

export enum Status {Schedule, Run, Done}
