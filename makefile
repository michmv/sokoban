all: source source/Main.js source/style.css

source/Main.js: src/Main.ts src/Types.ts src/Levels.ts src/Maybe.ts src/App.ts src/Animation.ts
	tsc --module amd src/Main.ts --outFile source/Main.js -sourcemap
	nodejs postmake.js

source/style.css: src/style.less
	lessc src/style.less > source/style.css

source:
	mkdir source
	cp -r ./static/* ./source

min: all
	uglifyjs source/Main.js -o source/Main.js -c -m
	csso source/style.css -o source/style.css
	rm -rf source/Main.js.map

clean:
	rm -rf source
