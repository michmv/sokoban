var fs = require('fs');

var file = __dirname + '/source/Main.js'

var result = ""

result = fs.readFileSync(file, function (err, data) {
    if (err) {
      throw err; 
    }
});

result = removePathTHREE(removeTHREE(result.toString()))

fs.writeFile(file, result, function(err) {
    if(err) {
        throw err; 
    }

    console.log("File source/Main.js was updated!");
}); 

function removeTHREE(str) {
    return str.replace(/(\(require, exports.*), THREE(,? ?.*\))/gm, '$1$2')
}

function removePathTHREE(str) {
    return str.replace(/, "\.\.\/three\/Three\"/gm, '')
}