# Sokoban
[Release](https://bitbucket.org/michmv/sokoban/src/release)
[Demo](http://mmv-module.herokuapp.com/demos/sokoban/)

Clone game.

## Build
Before need install:

* typescript
* lessc

```
$ git clone https://bitbucket.org/michmv/sokoban.git
$ cd sokoban
$ npm update
$ make
```

## Use

Open in browser `index.html`.
